# cz-pasaja-usercontrols v1.0.1

> WUI Framework library focused on user controls designed for project PASAJA.

## Requirements

This library does not have any special requirements, but it depends on
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder), so please see the WUI Builder's requirements, before you will
try to build this project.

## Project Build

Don't you want to waste your time with project build? OK! Project build is fully automated. For more information about project build see
the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically generated documentation in [JSDoc](http://usejsdoc.org/) from TypeScript source by running the `docs`
command.

> NOTE: Documentation can be accessible also from {projectRoot}/build/target/docs/index.html file after successful creation.

## History

### v1.0.0
Initial release
### v1.0.1
WUI namespaces refactoring

## License

This software is owned or controlled by PASAJA Authors. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar, Copyright (c) 2017 [PASAJA Authors](http://www.pasaja.cz/)
