Release Name: cz-pasaja-usercontrols v1.0.1

cz-pasaja-usercontrols
Description: WUI Framework user controls designed for project PASAJA
Author: PASAJA Authors
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: source, resource, test

com-wui-framework-commons v2.0.0
Description: Commons library for WUI Framework front-end projects.
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: dependencies

com-wui-framework-gui v2.0.0
Description: WUI Framework base GUI library
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: dependencies

Oxygen Font
Description: Web font in ttf format
Author: Vernon Adams
License: OFL-1.1, See OFL.txt
Format: font
Location: resource/libs/FontOxygen

com-wui-framework-usercontrols v2.0.0
Description: WUI Framework library focused on basic User Controls
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: dependencies
