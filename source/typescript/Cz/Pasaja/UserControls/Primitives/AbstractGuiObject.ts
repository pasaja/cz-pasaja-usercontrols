/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/**
 * @namespace Cz.Pasaja.UserControls.Primitives
 */
namespace Cz.Pasaja.UserControls.Primitives {
    "use strict";

    /**
     * @typedef {Object} AbstractGuiObject
     */

    export class AbstractGuiObject extends Com.Wui.Framework.UserControls.Primitives.AbstractGuiObject {

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return Com.Wui.Framework.UserControls.BaseInterface.Components.ToolTip;
        }
    }
}
