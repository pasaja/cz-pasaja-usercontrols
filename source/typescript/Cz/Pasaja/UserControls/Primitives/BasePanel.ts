/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Cz.Pasaja.UserControls.Primitives {
    "use strict";
    /**
     * @typedef {Object} BasePanel
     */

    import ToolTip = Com.Wui.Framework.UserControls.BaseInterface.Components.ToolTip;
    import ScrollBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar;
    import Icon = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Icon;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class BasePanel extends Com.Wui.Framework.UserControls.Primitives.BasePanel {
        constructor($id? : string) {
            super($id);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getLoaderIconClass() : any {
            return Icon;
        }

        /**
         * @return {IScrollBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar
         */
        protected getScrollBarClass() : any {
            return ScrollBar;
        }

        protected cssInterfaceName() : string {
            return BasePanel.ClassName();
        }
    }
}
