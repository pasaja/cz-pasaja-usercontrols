/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/**
 * @namespace Cz.Pasaja.UserControls.HttpProcessor
 */
namespace Cz.Pasaja.UserControls.HttpProcessor {
    "use strict";
    import EventsFactory = Com.Wui.Framework.UserControls.Events.EventsFactory;
    import HttpResolverArgs = Com.Wui.Framework.Commons.HttpProcessor.HttpResolverArgs;

    /**
     * @typedef {Object} HttpResolver
     */
    export class HttpResolver extends Com.Wui.Framework.Gui.HttpProcessor.HttpResolver {

        constructor($args? : HttpResolverArgs) {
            EventsFactory.getEventsManager();
            super($args);
        }

        protected getStartupResolvers() : any {
            let resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Cz.Pasaja.UserControls.Index;
            resolvers["/index"] = Cz.Pasaja.UserControls.Index;
            resolvers["/web/"] = Cz.Pasaja.UserControls.Index;
            return resolvers;
        }
    }
}
