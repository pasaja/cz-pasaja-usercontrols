/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Cz.Pasaja.UserControls.BaseInterface.UserControls {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ImageType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.ImageType;
    import Image = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Image;
    import ImageOutputArgs = Com.Wui.Framework.Gui.ImageProcessor.ImageOutputArgs;
    import Button = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Button;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import NumberPicker = Com.Wui.Framework.UserControls.BaseInterface.UserControls.NumberPicker;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import NumberPickerEventArgs = Com.Wui.Framework.Gui.Events.Args.NumberPickerEventArgs;
    import Label = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Label;
    import CropBox = Com.Wui.Framework.UserControls.BaseInterface.Components.CropBox;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import CropBoxEventArgs = Com.Wui.Framework.Gui.Events.Args.CropBoxEventArgs;
    import ImageCropDimensions = Com.Wui.Framework.Gui.Structures.ImageCropDimensions;

    export class ImageEditor extends Com.Wui.Framework.Gui.Primitives.FormsObject {

        private image : Image;
        private outputArgs : ImageOutputArgs;
        private zoomLabel : Label;
        private zoomPicker : NumberPicker;
        private rotateLabel : Label;
        private rotatePicker : NumberPicker;
        private brightnessLabel : Label;
        private brightnessPicker : NumberPicker;
        private contrastLabel : Label;
        private contrastPicker : NumberPicker;
        private cropButton : Button;
        private resetButton : Button;
        private cropBox : CropBox;
        private cropDimensions : ImageCropDimensions;

        private static onZoomChangeEventHandler($eventArgs : NumberPickerEventArgs, $manager : GuiObjectManager,
                                                $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>NumberPicker)) {
                let element : NumberPicker = <NumberPicker>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, NumberPicker)) {
                    let parent : ImageEditor = <ImageEditor>element.Parent();
                    if ($reflection.IsMemberOf(parent, ImageEditor)) {
                        element.getEvents().FireAsynchronousMethod(function () : void {
                            parent.ZoomImage($eventArgs.CurrentValue());
                        }, 100);
                    }
                }
            }
        }

        private static onRotateChangeEventHandler($eventArgs : NumberPickerEventArgs, $manager : GuiObjectManager,
                                                  $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>NumberPicker)) {
                let element : NumberPicker = <NumberPicker>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, NumberPicker)) {
                    let parent : ImageEditor = <ImageEditor>element.Parent();
                    if ($reflection.IsMemberOf(parent, ImageEditor)) {
                        element.getEvents().FireAsynchronousMethod(function () : void {
                            parent.RotateImage($eventArgs.CurrentValue());
                        }, 100);
                    }
                }
            }
        }

        private static onBrightnessChangeEventHandler($eventArgs : NumberPickerEventArgs, $manager : GuiObjectManager,
                                                      $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>NumberPicker)) {
                let element : NumberPicker = <NumberPicker>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, NumberPicker)) {
                    let parent : ImageEditor = <ImageEditor>element.Parent();
                    if ($reflection.IsMemberOf(parent, ImageEditor)) {
                        element.getEvents().FireAsynchronousMethod(function () : void {
                            parent.setImageBrightness($eventArgs.CurrentValue());
                        }, 100);
                    }
                }
            }
        }

        private static onContrastChangeEventHandler($eventArgs : NumberPickerEventArgs, $manager : GuiObjectManager,
                                                    $reflection : Reflection) : void {
            if ($manager.IsActive(<ClassName>NumberPicker)) {
                let element : NumberPicker = <NumberPicker>$eventArgs.Owner();
                if ($reflection.IsMemberOf(element, NumberPicker)) {
                    let parent : ImageEditor = <ImageEditor>element.Parent();
                    if ($reflection.IsMemberOf(parent, ImageEditor)) {
                        element.getEvents().FireAsynchronousMethod(function () : void {
                            parent.setImageContrast($eventArgs.CurrentValue());
                        }, 100);
                    }
                }
            }
        }

        private static onCropButtonClickEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager,
                                                     $reflection : Reflection) : void {

            let element : Button = <Button>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, Button)) {
                let parent : ImageEditor = <ImageEditor>element.Parent();
                if ($reflection.IsMemberOf(parent, ImageEditor)) {
                    parent.ToggleCropAction();
                }
            }
        }

        private static onResetButtonClickEventHandler($eventArgs : EventArgs, $manager : GuiObjectManager,
                                                      $reflection : Reflection) : void {
            let element : Button = <Button>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, Button)) {
                let parent : ImageEditor = <ImageEditor>element.Parent();
                if ($reflection.IsMemberOf(parent, ImageEditor)) {
                    parent.CleanChanges();
                }
            }
        }

        private static onCropBoxResizeCompleteEventHandler($eventArgs : CropBoxEventArgs, $manager : GuiObjectManager,
                                                           $reflection : Reflection) : void {
            let element : CropBox = <CropBox>$eventArgs.Owner();
            if ($reflection.IsMemberOf(element, CropBox)) {
                let parent : ImageEditor = <ImageEditor>element.Parent();
                if ($reflection.IsMemberOf(parent, ImageEditor)) {
                    parent.setCropDimensions($eventArgs);
                }
            }
        }

        constructor($id? : string) {
            super($id);
            this.outputArgs = new ImageOutputArgs();
            this.image = new Image(null, ImageType.GENERAL);
            this.zoomLabel = new Label();
            this.zoomPicker = new NumberPicker();
            this.rotateLabel = new Label();
            this.rotatePicker = new NumberPicker();
            this.brightnessLabel = new Label();
            this.brightnessPicker = new NumberPicker();
            this.contrastLabel = new Label();
            this.contrastPicker = new NumberPicker();
            this.cropButton = new Button();
            this.resetButton = new Button();
            this.cropBox = new CropBox();
            this.cropDimensions = new ImageCropDimensions(100, 100, 400, 400);
        }

        public setImageSource($source : string) : void {
            this.image.Source($source);
        }

        public RotateImage($deg : number) : void {
            this.outputArgs.Rotation($deg);
            this.image.OutputArgs(this.outputArgs);
        }

        public ZoomImage($percentage : number) : void {
            this.outputArgs.Zoom($percentage);
            this.image.OutputArgs(this.outputArgs);
        }

        public setImageBrightness($value : number) : void {
            this.outputArgs.Brightness($value);
            this.image.OutputArgs(this.outputArgs);
        }

        public setImageContrast($value : number) : void {
            this.outputArgs.Contrast($value);
            this.image.OutputArgs(this.outputArgs);
        }

        public setCropDimensions($eventArgs : CropBoxEventArgs) : void {
            this.cropDimensions.setDimensions($eventArgs.OffsetLeft(), $eventArgs.OffsetTop(),
                $eventArgs.OffsetLeft() + $eventArgs.Width(), $eventArgs.OffsetTop() + $eventArgs.Height());
        }

        public ToggleCropAction() : void {
            /// TODO: move texts outside of ImageEditor
            let isCropBoxVisible : boolean = this.cropBox.Visible();
            this.cropBox.Visible(!isCropBoxVisible);
            if (isCropBoxVisible) {
                this.outputArgs.CropDimension(
                    this.cropDimensions.TopX(), this.cropDimensions.TopY(),
                    this.cropDimensions.BottomX(), this.cropDimensions.BottomY()
                );
                this.image.OutputArgs(this.outputArgs);
                this.cropButton.Text("Crop");
            } else {
                this.outputArgs.CropDimension(0, 0, 0, 0);
                this.image.OutputArgs(this.outputArgs);
                this.cropButton.Text("Finish cropping");
            }
        }

        public CleanChanges() : void {
            this.outputArgs = new ImageOutputArgs();
            this.outputArgs.FillEnabled(false);
            this.image.OutputArgs(this.outputArgs);
            this.zoomPicker.Value(100);
            this.rotatePicker.Value(0);
            this.brightnessPicker.Value(0);
            this.contrastPicker.Value(0);
        }

        protected innerCode() : IGuiElement {
            this.outputArgs.Brightness(0);
            this.outputArgs.Contrast(0);
            this.outputArgs.FillEnabled(false);

            this.image.OutputArgs(this.outputArgs);
            this.image.setSize(640, 480);

            this.zoomPicker.RangeStart(100);
            this.zoomPicker.RangeEnd(150);
            this.zoomPicker.ValueStep(5);

            this.rotatePicker.RangeStart(0);
            this.rotatePicker.RangeEnd(360);
            this.rotatePicker.ValueStep(90);

            this.brightnessPicker.RangeStart(-255);
            this.brightnessPicker.RangeEnd(255);
            this.brightnessPicker.ValueStep(5);

            this.contrastPicker.RangeStart(-255);
            this.contrastPicker.RangeEnd(255);
            this.contrastPicker.ValueStep(5);

            this.cropBox.setDimensions(100, 100, 400, 400);
            this.cropBox.Visible(false);
            this.cropBox.EnvelopOwner(this.image);

            this.cropButton.Width(150);
            this.resetButton.Width(150);

            this.zoomPicker.getEvents().setOnChange(ImageEditor.onZoomChangeEventHandler);
            this.rotatePicker.getEvents().setOnChange(ImageEditor.onRotateChangeEventHandler);
            this.brightnessPicker.getEvents().setOnChange(ImageEditor.onBrightnessChangeEventHandler);
            this.contrastPicker.getEvents().setOnChange(ImageEditor.onContrastChangeEventHandler);
            this.cropButton.getEvents().setOnClick(ImageEditor.onCropButtonClickEventHandler);
            this.resetButton.getEvents().setOnClick(ImageEditor.onResetButtonClickEventHandler);
            this.cropBox.getEvents().setOnResizeComplete(ImageEditor.onCropBoxResizeCompleteEventHandler);

            /// TODO: move texts outside of ImageEditor
            this.zoomLabel.Value("zoom");
            this.zoomPicker.Text("{0} %");
            this.rotateLabel.Value("rotate");
            this.rotatePicker.Text("{0} °");
            this.brightnessLabel.Value("set brightness");
            this.brightnessPicker.Text("{0}");
            this.contrastLabel.Value("set contrast");
            this.contrastPicker.Text("{0}");
            this.cropButton.Text("Crop image");
            this.resetButton.Text("Reset");

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.image)
                .Add(this.cropBox)
                .Add(this.addElement().StyleClassName("Controls")
                    .Add(this.addElement()
                        .Add(this.zoomLabel)
                        .Add(this.zoomPicker)
                    )
                    .Add(this.addElement()
                        .Add(this.rotateLabel)
                        .Add(this.rotatePicker)
                    )
                    .Add(this.addElement()
                        .Add(this.brightnessLabel)
                        .Add(this.brightnessPicker)
                    )
                    .Add(this.addElement()
                        .Add(this.contrastLabel)
                        .Add(this.contrastPicker)
                    )
                    .Add(this.addElement().StyleClassName("Buttons")
                        .Add(this.cropButton)
                        .Add(this.resetButton)
                    )
                );
        }
    }
}
