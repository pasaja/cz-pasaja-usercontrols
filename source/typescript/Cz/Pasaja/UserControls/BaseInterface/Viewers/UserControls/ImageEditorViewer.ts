/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Cz.Pasaja.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ImageEditor = Cz.Pasaja.UserControls.BaseInterface.UserControls.ImageEditor;
    import Image = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Image;

    export class ImageEditorViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new ImageEditor());
        }

        public getInstance() : ImageEditor {
            return <ImageEditor>super.getInstance();
        }

        /* dev:start */
        protected testImplementation($instance : ImageEditor) : void {
            $instance.getEvents().setOnComplete(function () : void {
                $instance.CleanChanges();
            });

            $instance.setImageSource("test/resource/graphics/img1.jpg");
        }

        /* dev:end */
    }
}
