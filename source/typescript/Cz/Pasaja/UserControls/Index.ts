/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Cz.Pasaja.UserControls {
    "use strict";
    import String = Com.Wui.Framework.Commons.Primitives.String;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Viewers = Cz.Pasaja.UserControls.BaseInterface.Viewers;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;

    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {
        private environment : Com.Wui.Framework.Commons.EnvironmentArgs;

        /**
         * @class Index
         * @classdesc Index request resolver class provides handling of web index page.
         * @memberOf Cz.Pasaja.UserControls
         */
        constructor(...$args : any[]) {
            super();
            this.environment = $args[0];
        }

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>PASAJA User Controls Library</h1>" +
                "<h3>WUI Framework's User Controls designed for project PASAJA.</h3>" +
                "<div class=\"Index\">";

            if (ObjectValidator.IsSet(this.environment) && this.environment.IsBeta()) {
                output += "<H3>This is BETA version, please be aware of that coverage and selenium test have been skipped!</H3>";
            }

            output +=
                "<H3>Components</H3>" +
                String.NewLine() +

                "<H3>User Controls</H3>" +
                "<a href=\"" + Viewers.UserControls.ImageEditorViewer.CallbackLink(true) + "\">Image Editor</a>" +
                String.NewLine() +

                String.NewLine();

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +

                String.NewLine();
            /* dev:end */

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.environment.getProjectVersion() +
                ", build: " + this.environment.getBuildTime() +
                "</div>" + String.NewLine(false) +
                "<div class=\"Logo\">" + String.NewLine(false) +
                "   <div class=\"PASAJA\"></div>" + String.NewLine(false) +
                "   <div class=\"WUI\"></div>" + String.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("PASAJA - UserControls Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
