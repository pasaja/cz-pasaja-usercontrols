/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/**
 * @namespace Cz.Pasaja.UserControls
 */
namespace Cz.Pasaja.UserControls {
    "use strict";
    import HttpManager = Com.Wui.Framework.Gui.HttpProcessor.HttpManager;
    import HttpResolver = Cz.Pasaja.UserControls.HttpProcessor.HttpResolver;
    import HttpResolverArgs = Com.Wui.Framework.Commons.HttpProcessor.HttpResolverArgs;

    /**
     * @typedef {Object} Loader
     */

    /**
     * @class Loader
     * @classdesc Loader class provides handling of web content.
     * @memberOf Cz.Pasaja.UserControls
     */
    export class Loader extends Com.Wui.Framework.Commons.Loader {

        public static Load() : void {
            Com.Wui.Framework.Commons.Utils.Reflection.setInstanceNamespaces("Com.Wui", "Cz.Pasaja");
            Com.Wui.Framework.Commons.Loader.Load(
                new HttpResolver(
                    new HttpResolverArgs(
                        Com.Wui.Framework.Commons.Environment.getProjectName(),
                        HttpManager)));
        }
    }
}
